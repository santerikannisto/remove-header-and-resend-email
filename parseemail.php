#!/usr/local/bin/php -q
<?php

$authenticate='AUTHENTICATION STRING'; // this string has to be present in the email BODY to authenticate the sender, otherwise email is not forwarded
$from='FROM ADDRESS'; // replace with your FROM address
if ($debug) 
	$to='DEBUG TO ADDRESS'; // replace with your TO address
else
	$to='TO ADDRESS'; // replace with your from address
// DO NOT MODIFY BELOW THIS LINE
$authentic=false;
$debug=false;
$header=true;
$contenttype='Content-Type';
$subject='Subject: '; // Find email subject and put it to this variable
$body='';

fopen("php://stdin", "r");
while (false !== ($line = fgets(STDIN))) {
	if ($header) {
		// Find message subject
		if ( strpos( $line, $contenttype ) !== false)
			$contenttype=$line;
		if ( strpos( $line, $subject ) !== false)
			$subject=str_replace($subject, '', $line);
		if (strlen($line) == 1)
			$header=false;
	} else {
		if ( strpos( $line, $authenticate ) !== false) {
			$authentic=true;
		}
		$body .= $line;
	}
	if ($debug) echo strlen($line) . ' : ' . $line;
}

if ($debug) {
	echo $contenttype;
	echo $subject;
	echo '';
	echo $body;
}

$header="From: $from\r\nReply-To: $from\r\nMIME-Version: 1.0\r\n$contenttype\r\n";

if ($authentic)
	mail($to,$subject,$body,$header);

?>
